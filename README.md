### Usage

install `mammoth` and `python-docx` first
```shell
pip install mammoth
pip install python-docx
```

```shell
python process_docx.py <filename> <filename2>
```
