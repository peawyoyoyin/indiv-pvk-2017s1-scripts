from docx import Document
import re
import mammoth
import sys
import os

def extract_text(filename):
    print('extracting text from ' + filename)
    print('reading docx file')
    doc = Document(filename)
    file_without_ext = filename.split('/')[-1].split('.')[0]
    outfile = file_without_ext + '_extracted_texts.txt'
    out = open(outfile, 'w+', encoding='utf8')
    print('writing out to ' + outfile)
    for p in doc.paragraphs:
        out.write(p.text)
        out.write('\n')
    out.close()
    print('done')

def extract_table(filename, warn=False):
    print('extracting tables from ' + filename)
    print('converting docx to html using mammoth')
    with open(filename, 'rb') as f:
        result = mammoth.convert_to_html(f)
        text = result.value
        if warn:
            print(result.messages)
    print('using regex to detect tables')
    regex = r'<table[^>]*>.*?</table>'
    tables = re.findall(regex, text)
    
    actual_filename = filename.split('/')[-1].split('.')[0]
    out_folder = actual_filename + '_extracted_tables'
    if not os.path.exists(out_folder):
        os.mkdir(out_folder)
    currpath = os.getcwd()
    for index, table in enumerate(tables):
        fileout = str(index+1) + '.html'
        fullpath = os.path.join(currpath, out_folder, fileout)
        f = open(fullpath, 'x', encoding='utf8')
        print('writing to ' + fileout)
        f.write(table + '\n')
        f.close()

if __name__ == '__main__':
    for token in sys.argv[1:]:
        result_folder = token.split('/')[-1].split('.')[0] + '_results'
        if not os.path.exists(result_folder):
            os.mkdir(result_folder)
        os.chdir(result_folder)
        extract_table('../' + token)
        extract_text('../' + token)
        os.chdir('..')
